## CREATE AND CONTROL WINDOWS
~~~
// In the main process.
const {BrowserWindow} = require('electron')

// Or use `remote` from the renderer process.
// const {BrowserWindow} = require('electron').remote

let win = new BrowserWindow({width: 800, height: 600})
win.on('closed', () => {
  win = null
})

// Load a remote URL
win.loadURL('https://github.com')

// Or load a local HTML file
win.loadURL(`file://${__dirname}/app/index.html`)
~~~

## DRAG AND DROP
By default we can pretty much drag and select anything within the app, just like we would be able to do on a normal wesbite. For this we need to add some css rules. For example,                                          
~~~
-webkit-app-region: no-drag;                                                                                -webkit-app-region: drag;                                                                             -webkit-user-select: none; 
~~~

## DEFINING CONSTANTS
we can use constants for things we are not going to change. 
For example,                                   
~~~
const {app, BrowserWindow} = require('electron')                                                           
const path = require('path')                                                                             
const url = require('url')                                                                                  
~~~
## DEFINING VARIABLES
we are able to access variables in multiple code blocks, we need to define them outside of those code blocks. (this is known as variable scope).Using the keyword ```let``` we can define the variable name and this variable is then accessible from that code block and deeper. We can use ```win``` in other code blocks so we need to declare it here.
## DEFINING FUNCTIONS 
we can define a function as a seperate function, if we want to call it more than ones. By defining a function, we can do the following things: 
1. Set the titleBarStyle to hidden, macOS and linux apps have a title bar in their native default style. 
2. Set frame to false, this removes the top bar you see in Windows apps. 
3. Set the default size of the application with width and height and the minimum-width and minimum-height of the window size.

## USING EVENTS
we can use events in an application. For this we need to use the ```onclick```attribute. 
For example, if we have a javascript file called ```Showme()```then we would call it from out HTML. 
~~~
<input type="button" value="Click me!" onclick="ShowMe()">
~~~                                 

## GET INPUT FROM USER
We can get input from user in our app by using ```<input>```For example,
~~~
<input id="content" placeholder="type something">
~~~

## PRENT AND CHILD WINDOWS
By using ```parent``` option, we can create child windows:
~~~
const {BrowserWindow} = require('electron')                   
let top = new BrowserWindow()                                     
let child = new BrowserWindow({parent: top}                 
child.show()top.show()
~~~
The child window will always show on the the top of the top window.

## REFERENCE
1. https://electron.atom.io/docs/

2. http://to-bcs.nz/COMP6001/week02-electron

3. https://stackoverflow.com/questions/90813/best-practices-principles-for-gui-design
 